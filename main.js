    $( document ).ready(function() {

    // Variabel ============================================================
    var messenger_launcher = $('#messenger-launcher'),
        messenger_box = $('#messenger-box'),
        messenger_shadow = $('#messenger-shadow'),
        btn_close_messenger = $('#btn-close-messenger'),

        content = $('.messenger-box-content'),

        change_content = $('.change-messenger-content'),
        btn_home_content = $('#btn-home-messenger-content');

    // function animate css ===================================================
    $.fn.extend({
      animateCss: function(animationName, callback) {
        var animationEnd = (function(el) {
          var animations = {
            animation: 'animationend',
            OAnimation: 'oAnimationEnd',
            MozAnimation: 'mozAnimationEnd',
            WebkitAnimation: 'webkitAnimationEnd',
          };

          for (var t in animations) {
            if (el.style[t] !== undefined) {
              return animations[t];
            }
          }
        })(document.createElement('div'));

        this.addClass('animated ' + animationName).one(animationEnd, function() {
          $(this).removeClass('animated ' + animationName);

          if (typeof callback === 'function') callback();
        });

        return this;
      },
    });

    // auto height messenger body from top
    function auto_height_messenger_body(){
    var box_h = $('.messenger-box-header'),
        box_b = $('.messenger-box-body'),
        top_h = box_h.outerHeight(),
        box_h_height = $('.messenger-box-header-text').outerHeight();
           
        // top_h += value;
        box_h_height += 0;

        box_h.animate({height : box_h_height}, "fast", "linear");
        box_b.animate({paddingTop : box_h_height}, "fast", "linear");
        console.log(box_h_height);
    }

    // animated_messenger =====================================================================

    function animated_messenger($this){
        if ($this.hasClass('active')) {
            messenger_box.animateCss('fadeOutDown', 
                function(){
                    messenger_box.toggleClass('d-none')
                });
        }else{
            messenger_box.toggleClass('d-none')
            messenger_box.animateCss('fadeInUp');
        }
        $this.toggleClass('active');
        messenger_shadow.toggleClass('d-none');
    }


    // script ==================================================================================


    // launcher script open close icon ======================================

    messenger_launcher.click(function(event) {
        animated_messenger($(this));

    });

    btn_close_messenger.click(function(event){
        animated_messenger(messenger_launcher);
    });





    });